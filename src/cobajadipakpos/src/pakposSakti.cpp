#include "ros/ros.h"
#include <cobajadipakpos/kurir.h>

ros::Publisher pakposKurir;

float perangkos, surats, belanjas;


int main(int argc, char **argv){
  ros::init(argc, argv, "pakposSakti");

  ros::NodeHandle n;
  pakposKurir = n.advertise<cobajadipakpos::kurir>("/kurir_sakti",100);
  cobajadipakpos::kurir kurir;
  ros::Rate loop_rate(1000);
  while(ros::ok()){
        
        perangkos +=1;
        surats +=2;
        belanjas +=3;

        kurir.perangko = perangkos;
        kurir.surat = surats;
        kurir.belanja = belanjas;

        pakposKurir.publish(kurir);

        ROS_INFO("\nperangko: %f \nsurat: %f \nbelanja: %f \n", perangkos, surats, belanjas);
        ros::spinOnce();
        loop_rate.sleep();
    } 
    return 0; 
}
